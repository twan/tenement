package com.mm.dss.demo;

import android.app.Application;

import com.facebook.soloader.SoLoader;

/**
 * @author Twan
 * @date 2019/5/26
 */
public class Dssdk {

    public static void init(Application context){
        try {
            SoLoader.init(context,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SoLoader.loadLibrary("gnustl_shared");
        SoLoader.loadLibrary("dsl");
        SoLoader.loadLibrary("dslalien");
        SoLoader.loadLibrary("DPRTSPSDK");
        SoLoader.loadLibrary("PlatformRestSDK");
        SoLoader.loadLibrary("PlatformSDK");
        SoLoader.loadLibrary("netsdk");
        SoLoader.loadLibrary("CommonSDK");
    }
}
