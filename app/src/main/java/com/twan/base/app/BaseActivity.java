package com.twan.base.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jaeger.library.StatusBarUtil;
import com.twan.base.R;
import com.twan.base.util.ShareUtil;
import com.twan.swipebacklayout.app.SwipeBackActivity;
import com.twan.base.ui.MainActivity;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;


public abstract class BaseActivity extends SwipeBackActivity {
    protected Activity mContext;

    @BindView(R.id.back) public ImageButton back;
    @BindView(R.id.tv_webview_close) public TextView tv_webview_close;
    @BindView(R.id.more)  public  ImageButton ib_more;
    @BindView(R.id.title) public TextView title;
    @BindView(R.id.moreText) public TextView moreText;
    @BindView(R.id.toolbar) public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        setSupportActionBar(toolbar);
        setStatusBar();
        ButterKnife.bind(this);
        mContext = this;
        App.getInstance().addActivity(this);
        initTitle();
        initEventAndData();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getInstance().removeActivity(this);
    }

    protected void setStatusBar(){
        StatusBarUtil.setColor(this, getColor(R.color.app_blue), 128);
    }

    protected void initTitle(){
        back.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        tv_webview_close.setVisibility(View.GONE);
        tv_webview_close.setText("返回");
        title.setText(getString(R.string.app_name));
        ib_more.setVisibility(View.GONE);
        moreText.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.this.finish();
            }
        });

    }


    protected abstract int getLayout();

    protected abstract void initEventAndData();

}
