package com.twan.base.fragment.main;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.twan.base.R;
import com.twan.base.app.BaseFragment;
import com.twan.base.entity.LoginBean;
import com.twan.base.ui.ForgetPasswordActivity;
import com.twan.base.ui.LoginActivity;
import com.twan.base.ui.MyFamilyActivity;
import com.twan.base.ui.MyFeeActivity;
import com.twan.base.ui.MyMessageActivity;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.MyUtil;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;
import com.twan.mylibary.router.Router;

import butterknife.BindView;
import butterknife.OnClick;

import static com.twan.base.app.Constants.SP_CUSTOM_ID;
import static com.twan.base.app.Constants.SP_LOGIN_BEAN;

/**
 * Created by twan on 2017/10/26.
 * 个人中心
 *
 */
public class MyFragment extends BaseFragment {
    @BindView(R.id.tv_my_name) TextView tv_my_name;
    @BindView(R.id.tv_mobile) TextView tv_mobile;
    @BindView(R.id.tv_hao) TextView tv_hao;
    @BindView(R.id.tv_house_square) TextView tv_house_square;

    private LoginBean mData;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        String loignjson = SPUtils.getInstance().getString(SP_LOGIN_BEAN);
        mData = JsonUtil.jsonToBean(loignjson, LoginBean.class);

        if (mData != null) {
            tv_my_name.setText(mData.getName());
            tv_mobile.setText(mData.getPhone());
            tv_hao.setText(mData.getAddress());
            tv_house_square.setText(mData.getSize());
        }
    }

    @Override
    protected void initData(Bundle arguments) {

    }

    @OnClick({R.id.rl_my_family,R.id.rl_my_fee,R.id.rl_my_msg,R.id.btn_logout,R.id.rl_contact})
    public void doOnclick(View view){
        switch (view.getId()){
            case R.id.rl_my_family:
                Router.newIntent(mActivity).to(MyFamilyActivity.class).launch();
                break;
            case R.id.rl_my_fee:
                Router.newIntent(mActivity).to(MyFeeActivity.class).launch();
                break;
            case R.id.rl_my_msg:
                Router.newIntent(mActivity).to(MyMessageActivity.class).launch();
                break;
            case R.id.btn_logout:
                new XPopup.Builder(getActivity()).asConfirm("提示", "您要退出登录吗?", new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        SPUtils.getInstance().put(SP_CUSTOM_ID,"");
                        Router.newIntent(getActivity()).to(LoginActivity.class).launch();
                        getActivity().finish();
                        ToastUtil.success("退出成功!");
                    }
                }).show();
                break;
            case R.id.rl_contact:
                MyUtil.callPhone(getActivity(),mData.getKefu());
                break;
        }
    }
}
