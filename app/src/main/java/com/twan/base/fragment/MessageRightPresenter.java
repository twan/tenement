package com.twan.base.fragment;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.entity.MessageBean;
import com.twan.base.entity.MyMessage;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Twan
 * @date 2019/4/27
 */
public class MessageRightPresenter {

    private Activity mContext;
    SmartRefreshLayout mRefreshLayout;
    RecyclerView mRecyclerView;
    List<MessageBean> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<MessageBean> mAdpater;
    private int pageIndex =1;

    public MessageRightPresenter(View view, Activity activity) {
        mContext = activity;
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        initRefresh();
        initData();
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<MessageBean>(mddddd, R.layout.item_message_right) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, MessageBean model, final int position) {
                //GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                ImageView iv_red_hot = holder.itemView.findViewById(R.id.iv_red_hot_my);
                holder.text(R.id.tv_title, model.getTitle());
                holder.text(R.id.tv_desc, model.getMemo());
                holder.text(R.id.tv_time, model.getPudate());
                if (model.getZt().equals("1")){
                    iv_red_hot.setVisibility(View.VISIBLE);
                } else {
                    iv_red_hot.setVisibility(View.GONE);
                }
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                initData();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                initData();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(.class).launch();
                ImageView iv_red_hot = view.findViewById(R.id.iv_red_hot_my);

                MessageBean model = mddddd.get(position);
                if (model.getZt().equals("1")) {//消息状态：1.未读 2.已读
                    editMessage(iv_red_hot,mddddd.get(position).getId());
                }
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    protected void initData() {
        Api.getApiService().getMess(SPUtils.getCustomId(), "1",pageIndex + "").enqueue(new ZyCallBack<MyMessage>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyMessage bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }

    private void editMessage(final ImageView iv_red_hot,String id){
        Api.getApiService().editMess(SPUtils.getCustomId(), id).enqueue(new ZyCallBack() {
            @Override
            public void doSuccess() {
                iv_red_hot.setVisibility(View.GONE);
            }
        });
    }
}
