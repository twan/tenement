package com.twan.base.fragment;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.entity.FeeBean;
import com.twan.base.entity.MessageBean;
import com.twan.base.entity.MyFamilyBean;
import com.twan.base.entity.MyFee;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Twan
 * @date 2019/4/27
 */
public class FeeLeftPresenter {
    private Activity mContext;
    SmartRefreshLayout mRefreshLayout;
    RecyclerView mRecyclerView;
    List<FeeBean> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<FeeBean> mAdpater;
    private int pageIndex =1;

    public FeeLeftPresenter(View view, Activity activity) {
        mContext = activity;
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        initRefresh();
        initData();
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<FeeBean>(mddddd, R.layout.item_fee_left) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, FeeBean model, final int position) {
                //GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_date1, model.getDate1());
                holder.text(R.id.tv_date2, model.getDate2());
                holder.text(R.id.tv_money, model.getMoney());
                holder.text(R.id.tv_zffs, model.getZffs().equals("1")?"微信":"支付宝");
                holder.text(R.id.tv_zfDate, model.getZfDate());
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                initData();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                initData();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(.class).launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    protected void initData() {
        Api.getApiService().getWuYeFeiJiLu(SPUtils.getCustomId(), pageIndex + "").enqueue(new ZyCallBack<MyFee>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyFee bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }
}
