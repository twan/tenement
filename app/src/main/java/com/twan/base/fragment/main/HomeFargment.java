package com.twan.base.fragment.main;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.stx.xhb.xbanner.XBanner;
import com.stx.xhb.xbanner.transformers.Transformer;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.HomeGridViewAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseFragment;
import com.twan.base.entity.AdertisBean;
import com.twan.base.entity.GridIcon;
import com.twan.base.entity.LoginBean;
import com.twan.base.entity.MyIndexBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.ui.ConvenServiceActivity;
import com.twan.base.ui.MainActivity;
import com.twan.base.ui.MaintainActivity;
import com.twan.base.ui.MonitorActivity;
import com.twan.base.ui.ParkFeeActivity;
import com.twan.base.ui.ShopActivity;
import com.twan.base.ui.TenementFeeActivity;
import com.twan.base.ui.WaterFeeActivity;
import com.twan.base.ui.WebViewActivity;
import com.twan.base.util.GlideUtils;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.LogUtil;
import com.twan.base.util.SPUtils;
import com.twan.mylibary.router.Router;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.twan.base.app.Constants.SP_LOGIN_BEAN;

/**
 * Created by twan on 2017/10/26.
 */
public class HomeFargment extends BaseFragment {

    @BindView(R.id.refreshLayout) RefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.gridview) GridView gridView;
    @BindView(R.id.tv_notice) TextView tv_notice;
    @BindView(R.id.tv_house_name) TextView tv_house_name;
    @BindView(R.id.banner) XBanner mBanner;

    ArrayList<GridIcon> mGridData;
    HomeGridViewAdapter mGridAdapter;

    private BaseRecyclerAdapter<AdertisBean.Adv> mAdpater;
    List<AdertisBean.Adv> mddddd = new ArrayList<>();
    List<MyIndexBean.Banner> mBannerList = new ArrayList<>();
    private int pageIndex =1;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        //设置小区名字
        String loignjson = SPUtils.getInstance().getString(SP_LOGIN_BEAN);
        LoginBean bean = JsonUtil.jsonToBean(loignjson, LoginBean.class);
        if (bean != null) {
            tv_house_name.setText(bean.getXiaoqu());
        }

        initGridView();
        initRecyclerView();
        initBanner();
        getBanner();
    }

    private void initBanner(){
        mBanner.setPageTransformer(Transformer.Alpha);
        mBanner.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner banner, Object model, View view, int position) {
                //Toast.makeText(ClipChildrenModeActivity.this, "点击了第" + (position + 1) + "图片", Toast.LENGTH_SHORT).show();
            }
        });
        mBanner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                //此处适用Fresco加载图片，可自行替换自己的图片加载框架
//                ImageView draweeView = (ImageView) view;
//                MyIndexBean.Banner bean = ((MyIndexBean.Banner) model);
////                String url = "https://photo.tuchong.com/" + listBean.getImages().get(0).getUser_id() + "/f/" + listBean.getImages().get(0).getImg_id() + ".jpg";
////                draweeView.setImageURI(Uri.parse(url));
//                LogUtil.d("图片地址"+bean.getUrl());
                // https://photo.tuchong.com/445693/f/11999461.jpg
                // https://photo.tuchong.com/1056197/f/26695201.jpg
//                GlideUtils.loadRoundCorner(getActivity(),draweeView,bean.getUrl());


                SimpleDraweeView draweeView = (SimpleDraweeView) view;
                MyIndexBean.Banner bean = ((MyIndexBean.Banner) model);
                draweeView.setImageURI(Uri.parse(bean.getImg()));
            }
        });
    }

    private void initRecyclerView(){
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<AdertisBean.Adv>(mddddd, R.layout.item_index_adv) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, AdertisBean.Adv model, final int position) {
                GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_adv),model.getImg());
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                onLazyLoad();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                onLazyLoad();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Router.newIntent(getActivity()).to(WebViewActivity.class)
                        .putString("title",mddddd.get(position).getTitle())
                        .putString("url",mddddd.get(position).getUrl())
                        .launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    private void initGridView(){
        mGridData = new ArrayList<>();
        mGridData.add(new GridIcon(R.mipmap.water, "水费代缴"));
        mGridData.add(new GridIcon(R.mipmap.notepad, "小区公告"));
        mGridData.add(new GridIcon(R.mipmap.maintain, "业主报修"));

        mGridData.add(new GridIcon(R.mipmap.tenement_pay, "物业缴费"));
        mGridData.add(new GridIcon(R.mipmap.service, "便民服务"));
        mGridData.add(new GridIcon(R.mipmap.monitor, "监控中心"));
        mGridData.add(new GridIcon(R.mipmap.shop, "商城"));
        //mGridData.add(new GridIcon(R.mipmap.spark_pay, "停车缴费"));

        gridView.setAdapter(mGridAdapter =new HomeGridViewAdapter(mContext,R.layout.item_grid,mGridData));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0://水费代缴
                        Router.newIntent(mActivity).to(WaterFeeActivity.class).launch();
                        break;
                    case 1://小区公告
                        ((MainActivity)mActivity).setViewPagerItem(2);
                        break;
                    case 2://业主报修
                        Router.newIntent(mActivity).to(MaintainActivity.class).launch();
                        break;
                    case 3://物业缴费
                        Router.newIntent(mActivity).to(TenementFeeActivity.class).launch();
                        break;
                    case 4://便民服务
                        Router.newIntent(mActivity).to(ConvenServiceActivity.class).launch();
                        break;
                    case 5://监控中心
                        Router.newIntent(mActivity).to(MonitorActivity.class).launch();
                        break;
                    case 6://商城
                        Router.newIntent(mActivity).to(ShopActivity.class).launch();
                        break;
                    case 7://停车缴费
                        //Router.newIntent(mActivity).to(ParkFeeActivity.class).launch();
                        break;
                }
            }
        });
    }

    @OnClick({R.id.ll_notice})
    public void doOnclick(View view){
        switch (view.getId()){
            case R.id.ll_notice:
                ((MainActivity)mActivity).setViewPagerItem(2);
                break;
        }
    }

    @Override
    protected void onLazyLoad() {
        //广告
        Api.getApiService().getGg(SPUtils.getCustomId(),pageIndex + "").enqueue(new ZyCallBack<AdertisBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                AdertisBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    if (pageIndex ==1){
                        dataClear();
                    }
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });

    }

    private void getBanner(){
        //banner
        Api.getApiService().getIndex(SPUtils.getCustomId()).enqueue(new ZyCallBack<MyIndexBean>() {
            @Override
            public void doSuccess() {
                MyIndexBean bean = mData;
                tv_notice.setText(bean.getTitle());
                if (bean.getList() != null && bean.getList().size() > 0) {
                    mBannerList.clear();
                    mBannerList = bean.getList();
                    mBanner.setAutoPlayAble(mBannerList.size() > 1);
                    mBanner.setIsClipChildrenMode(false);
                    mBanner.setBannerData(R.layout.layout_banner_imageview, mBannerList);
                }
            }
        });
    }
}
