package com.twan.base.fragment.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.lxj.xpopup.XPopup;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseFragment;
import com.twan.base.entity.MyNoticeBean;
import com.twan.base.entity.MyStewardBean;
import com.twan.base.entity.StewardBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.ui.MaintainActivity;
import com.twan.base.ui.StewardEvalutionActivity;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;
import com.twan.base.widget.EvalutionCenterPop;
import com.twan.base.widget.SimpleTagImageView;
import com.twan.mylibary.router.Router;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by twan on 2017/10/26.
 * 天安管家
 */

public class StewardFragment extends BaseFragment {

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private int pageIndex =1;
    List<StewardBean> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<StewardBean> mAdpater;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_steward;
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        back.setVisibility(View.GONE);
        ib_more.setVisibility(View.VISIBLE);
        title.setText("天安管家");
    }

    @OnClick(R.id.more)
    public void doClick(View view){
        Router.newIntent(mActivity).to(MaintainActivity.class).launch();
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        initRefresh();
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<StewardBean>(mddddd, R.layout.item_steward) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, final StewardBean model, final int position) {
                //GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                Button btneval = holder.itemView.findViewById(R.id.btn_evalution);
                TextView tv_bxstatus = holder.itemView.findViewById(R.id.tv_bxstatus);
                SimpleTagImageView iv_todo = holder.itemView.findViewById(R.id.iv_todo);
                iv_todo.setTagOrientation(SimpleTagImageView.RIGHT_TOP);

                String zt = model.getZt();
                final String pjzt = model.getPjZt();//评价状态：1.未评价 2.已评价

                holder.text(R.id.tv_bxdesc, model.getDescribe());
                holder.text(R.id.tv_bxtime, model.getPudate());
                holder.text(R.id.tv_bxstatus, zt.equals("1")?"待处理":(zt.equals("2")?"已处理":"已撤销"));//报修状态：1.待处理 2.已处理 3.已撤销


                int clickStatus = 1000;
                if (zt.equals("1")){
                    clickStatus =1000;
                    btneval.setClickable(true);
                    btneval.setText("撤销报修");
                    btneval.setTextColor(Color.WHITE);
                    btneval.setBackgroundResource(R.drawable.corner_red_conrner_update);
                    tv_bxstatus.setTextColor(getResources().getColor(R.color.text_red));

                    iv_todo.setTagText("待处理");
                    iv_todo.setTagBackgroundColor(Color.RED);
                } else if (zt.equals("2")) {
                    if (pjzt.equals("1")) {
                        clickStatus =2000;
                        btneval.setClickable(true);
                        btneval.setText("评价");
                        btneval.setTextColor(Color.WHITE);
                        btneval.setBackgroundResource(R.mipmap.evalution);

                        iv_todo.setTagText("已处理");
                        iv_todo.setTagBackgroundColor(getResources().getColor(R.color.text_blue));
                    } else {
                        clickStatus =-1;
                        btneval.setText("已评价");
                        btneval.setClickable(false);
                        btneval.setTextColor(Color.BLACK);
                        btneval.setBackgroundResource(R.drawable.corner_gray_conrner_default);

                        iv_todo.setTagText("已处理");
                        iv_todo.setTagBackgroundColor(getResources().getColor(R.color.text_blue));
                    }
                } else {
                    clickStatus =-1;
                    btneval.setClickable(false);
                    btneval.setText("已撤销");
                    btneval.setTextColor(Color.BLACK);
                    btneval.setBackgroundResource(R.drawable.corner_gray_conrner_default);

                    iv_todo.setTagText("已取消");
                    iv_todo.setTagBackgroundColor(getResources().getColor(R.color.text_blue));
                }

                final int finalClickStatus = clickStatus;
                btneval.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (finalClickStatus ==2000) {
                            new XPopup.Builder(mContext)
                                    .dismissOnTouchOutside(true)
                                    .asCustom(new EvalutionCenterPop(mContext, model.getId(), new EvalutionCenterPop.IEvalution() {
                                        @Override
                                        public void onsucess() {
                                            dataClear();
                                            onLazyLoad();
                                        }
                                    }))
                                    .show();
                        }else if (finalClickStatus ==1000){
                            delBaoXiu(model.getId());
                        } else {
                            //不处理
                        }
                    }
                });
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                onLazyLoad();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                onLazyLoad();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StewardBean model = mddddd.get(position);
                Router.newIntent(mActivity).to(StewardEvalutionActivity.class)
                        .putString("pjzt", model.getPjZt())
                        .putString("zt", model.getZt())
                        .putString("id", model.getId()).launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    @Override
    protected void onLazyLoad() {
        Api.getApiService().getBaoXiu(SPUtils.getCustomId(),pageIndex + "").enqueue(new ZyCallBack<MyStewardBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyStewardBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    if (pageIndex ==1){
                        dataClear();
                    }
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }

    private void delBaoXiu(String id){
        Api.getApiService().delBaoXiu(SPUtils.getCustomId(),id).enqueue(new ZyCallBack() {
            @Override
            public void doSuccess() {
                ToastUtil.success("撤销成功");
                dataClear();
                onLazyLoad();
            }
        });
    }
}
