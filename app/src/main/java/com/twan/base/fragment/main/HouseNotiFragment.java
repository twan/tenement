package com.twan.base.fragment.main;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseFragment;
import com.twan.base.entity.AdertisBean;
import com.twan.base.entity.HouseNotiBean;
import com.twan.base.entity.MyNoticeBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.ui.ForgetPasswordActivity;
import com.twan.base.ui.LoginActivity;
import com.twan.base.ui.NotiDetailActivity;
import com.twan.base.ui.WebViewActivity;
import com.twan.base.util.GlideUtils;
import com.twan.base.util.SPUtils;
import com.twan.mylibary.router.Router;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by twan on 2017/10/26.
 */
public class HouseNotiFragment extends BaseFragment {

    @BindView(R.id.refreshLayout) SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;

    private int pageIndex =1;
    List<HouseNotiBean> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<HouseNotiBean> mAdpater;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_house_noti;
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("小区公告");
        back.setVisibility(View.GONE);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        initRefresh();
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<HouseNotiBean>(mddddd, R.layout.item_house_noti) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, HouseNotiBean model, final int position) {
                GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_desc, model.getTitle());
                holder.text(R.id.tv_date, model.getPudate());
                holder.text(R.id.tv_author, model.getZuozhe());
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                onLazyLoad();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                onLazyLoad();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(NotiDetailActivity.class).launch();
                Router.newIntent(getActivity()).to(WebViewActivity.class)
                        .putString("title","公告详情")
                        .putString("url",mddddd.get(position).getUrl())
                        .launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    @Override
    protected void onLazyLoad() {
        Api.getApiService().getGongGao(SPUtils.getCustomId(),pageIndex + "").enqueue(new ZyCallBack<MyNoticeBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyNoticeBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    if (pageIndex ==1){
                        dataClear();
                    }
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }
}
