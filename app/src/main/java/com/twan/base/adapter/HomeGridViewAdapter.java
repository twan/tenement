package com.twan.base.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twan.base.R;
import com.twan.base.entity.GridIcon;

import java.util.ArrayList;

/**
 * @author Twan
 * @date 2019/3/25
 */
public class HomeGridViewAdapter extends ArrayAdapter<GridIcon> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridIcon> mGridData = new ArrayList<>();
    private int selectorPosition;

    public HomeGridViewAdapter(Context context, int resource, ArrayList<GridIcon> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.layoutResourceId = resource;
        this.mGridData = objects;
    }

    public void setGridData(ArrayList<GridIcon> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.iv_image = (ImageView)convertView.findViewById(R.id.iv_image);
            holder.tv_desc = convertView.findViewById(R.id.tv_desc);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        GridIcon item = mGridData.get(position);
        holder.tv_desc.setText(item.getDesc());
        holder.iv_image.setImageResource(item.getRid());
        return convertView;
    }

    private class ViewHolder {
        ImageView iv_image;
        TextView tv_desc;
    }

    public void changeState(int pos) {
        selectorPosition = pos;
        notifyDataSetChanged();
    }

}
