package com.twan.base.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by hackware on 2016/9/10.
 */

public class MessagePagerAdapter extends PagerAdapter {
    private List<View> mDataList;

    public MessagePagerAdapter(List<View> dataList) {
        mDataList = dataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mDataList.get(position));
        return mDataList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mDataList.get(position));
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
//        TextView textView = (TextView) object;
//        String text = textView.getText().toString();
//        int index = mDataList.indexOf(text);
//        if (index >= 0) {
//            return index;
//        }
//        return POSITION_NONE;
    }
}
