package com.twan.base.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.twan.base.R;
import com.twan.base.entity.RelationBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Twan
 * @date 2019/3/25
 */
public class RelationGridViewAdapter extends ArrayAdapter<RelationBean.Relation> {

    private Context mContext;
    private int layoutResourceId;
    private List<RelationBean.Relation> mGridData = new ArrayList<>();


    public RelationGridViewAdapter(Context context, int resource, List<RelationBean.Relation> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.layoutResourceId = resource;
        this.mGridData = objects;
    }

    public void setGridData(ArrayList<RelationBean.Relation> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.desc);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        RelationBean.Relation item = mGridData.get(position);
        holder.textView.setText(item.getGxName());
        return convertView;
    }

    private class ViewHolder {
        TextView textView;
    }

}
