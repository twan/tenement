package com.twan.base.ui;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.MyFamilyBean;
import com.twan.base.entity.RelationBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.SPUtils;
import com.twan.mylibary.router.Router;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.twan.base.app.Constants.SP_RELATION;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class MyFamilyActivity extends BaseActivity {
    @BindView(R.id.refreshLayout) SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;

    private int pageIndex = 1;
    List<MyFamilyBean.Member> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<MyFamilyBean.Member> mAdpater;

    @Override
    protected int getLayout() {
        return R.layout.activity_my_family;
    }

    @Override
    protected void initEventAndData() {
        initRefresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataClear();
        initData(pageIndex);
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("我的家人");
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<MyFamilyBean.Member>(mddddd, R.layout.item_my_family) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, MyFamilyBean.Member model, final int position) {
                //GlideUtils.load(MyFamilyActivity.this,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_desc1,SPUtils.getRelationName(model.getGxId())+": "+model.getGxName());
                holder.text(R.id.tv_desc2,"电话: "+model.getPhone());
                //审核状态：1.审核中 2.已开通 3.被拒绝
                String stua;
                TextView textView = holder.itemView.findViewById(R.id.tv_status);
                if (model.getZt().equals("1")){
                    stua ="审核中";
                    textView.setBackgroundColor(getResources().getColor(R.color.text_red));
                }else if (model.getZt().equals("2")){
                    stua ="已开通";
                    textView.setBackgroundColor(getResources().getColor(R.color.text_green));
                }else {
                    stua ="被拒绝";
                    textView.setBackgroundColor(getResources().getColor(R.color.text_deep_gray));
                }
                holder.text(R.id.tv_status,stua);
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                initData(pageIndex);
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                initData(pageIndex);
            }
        });
    }

    private void dataClear() {
        pageIndex = 1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    private void initData(int pageindex) {
        Api.getApiService().myJia(SPUtils.getCustomId(), pageindex + "").enqueue(new ZyCallBack<MyFamilyBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyFamilyBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }


    @OnClick({R.id.btn_add_family})
    public void doOnclick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_family:
                Router.newIntent(MyFamilyActivity.this).to(AddFamilyActivity.class).launch();
                break;
        }
    }
}
