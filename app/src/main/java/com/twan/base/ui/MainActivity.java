package com.twan.base.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.twan.base.R;
import com.twan.base.adapter.IndicateAdapter;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.RelationBean;
import com.twan.base.fragment.main.StewardFragment;
import com.twan.base.fragment.main.HouseNotiFragment;
import com.twan.base.fragment.main.HomeFargment;
import com.twan.base.fragment.main.MyFragment;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.LogUtil;
import com.twan.base.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.twan.base.app.Constants.SP_RELATION;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bottomBar) BottomBar bottomBar;
    @BindView(R.id.mainboard) View mainboard;
    List<Fragment> views = new ArrayList<>();
    public static MainActivity instance;
    ViewPager viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setSwipeBackEnable(false);
    }

    @Override
    protected void initEventAndData() {
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        StewardFragment cashFragment = new StewardFragment();
        HomeFargment taskFargment = new HomeFargment();
        HouseNotiFragment clockFragment = new HouseNotiFragment();
        MyFragment myFragment = new MyFragment();
        views.clear();
        views.add(taskFargment);
        views.add(cashFragment);
        views.add(clockFragment);
        views.add(myFragment);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_index:
                        viewpager.setCurrentItem(0, true);
                        break;
                    case R.id.tab_steward:
                        viewpager.setCurrentItem(1, true);
                        break;
                    case R.id.tab_house:
                        viewpager.setCurrentItem(2, true);
                        break;
                    case R.id.tab_my:
                        viewpager.setCurrentItem(3, true);
                        break;
                }
            }
        });

        IndicateAdapter myFragmentAdapter = new IndicateAdapter(getSupportFragmentManager(), views);
        viewpager.setAdapter(myFragmentAdapter);
        viewpager.setCurrentItem(0);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {
                if (bottomBar != null) bottomBar.selectTabAtPosition(pos, true);
                if (pos == 0) {

                } else if (pos == 1) {

                } else if (pos == 2) {

                } else if (pos == 3) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        loadCachaData();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    public void setViewPagerItem(int index){
        viewpager.setCurrentItem(index, true);
    }

    private void loadCachaData(){
        //缓存关系数据
        if (TextUtils.isEmpty(SPUtils.getInstance().getString(SP_RELATION))) {
            Api.getApiService().getGx().enqueue(new ZyCallBack<RelationBean>() {
                @Override
                public void doSuccess() {
                    if (mData != null) {
                        LogUtil.d("获取家庭成员关系列表成功");
                        SPUtils.getInstance().put(SP_RELATION, JsonUtil.objectToJson(mData.getList()));
                    }
                }
            });
        }
    }

}
