package com.twan.base.ui;

import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lxj.xpopup.XPopup;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.ConvenServiceBean;
import com.twan.base.entity.MyNoticeBean;
import com.twan.base.entity.MyTenementBean;
import com.twan.base.entity.TenementFeeBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.SPUtils;
import com.twan.base.widget.EvalutionCenterPop;
import com.twan.base.widget.TenementDetailPop;
import com.twan.base.widget.TenementFeePop;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Twan
 * @date 2019/4/24
 *
 * 物业缴费
 */
public class TenementFeeActivity extends BaseActivity {
    @BindView(R.id.nsv_scrollview) NestedScrollView nsv_scrollview;
    @BindView(R.id.recycler_view1) RecyclerView recycler_view1;
    @BindView(R.id.recycler_view2) RecyclerView recycler_view2;
    @BindView(R.id.refreshLayout) SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.tv_title) TextView tv_title;
    @BindView(R.id.ll_notice) LinearLayout ll_notice;

    private BaseRecyclerAdapter<TenementFeeBean> mAdpater1;
    private BaseRecyclerAdapter<TenementFeeBean> mAdpater2;
    private List<TenementFeeBean> mddddd1 = new ArrayList<>();
    private List<TenementFeeBean> mddddd2 = new ArrayList<>();
    private int pageIndex = 1;

    @Override
    protected int getLayout() {
        return R.layout.activity_tenement_fee;
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("物业缴费");
    }

    @Override
    protected void initEventAndData() {
        initTitle();
        initRefresh();
        initRecyclerView2();
        initRecyclerView1();
        firstData();
        initData();
    }

    private void initRefresh(){
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                firstData();
                initData();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                initData();
            }
        });
    }
    private void initRecyclerView1() {
        recycler_view1.setLayoutManager(new LinearLayoutManager(mContext));
        recycler_view1.setItemAnimator(new DefaultItemAnimator());
        recycler_view1.setAdapter(mAdpater1 = new BaseRecyclerAdapter<TenementFeeBean>(mddddd1, R.layout.item_tenement_fee1) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, final TenementFeeBean model, final int position) {
                //GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_desc, model.getTitle());
                holder.text(R.id.tv_money, "¥ "+model.getMoney());

                //缴费
                holder.itemView.findViewById(R.id.btn_fee).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new XPopup.Builder(mContext)
                                .dismissOnTouchOutside(true)
                                .asCustom(new TenementFeePop(mContext, "2", model.getId(), new TenementFeePop.IPayListener() {
                                    @Override
                                    public void onsuccess() {
                                        firstData();
                                        initData();
                                    }
                                }))
                                .show();
                    }
                });

                //查看详细
                holder.itemView.findViewById(R.id.tv_detail).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new XPopup.Builder(mContext)
                                .dismissOnTouchOutside(true)
                                .asCustom(new TenementDetailPop(mContext,model))
                                .show();
                    }
                });
            }
        });
        mAdpater1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(NotiDetailActivity.class).launch();
            }
        });
    }

    private void initRecyclerView2() {
        recycler_view2.setLayoutManager(new LinearLayoutManager(mContext));
        recycler_view2.setItemAnimator(new DefaultItemAnimator());
        recycler_view2.setAdapter(mAdpater2 = new BaseRecyclerAdapter<TenementFeeBean>(mddddd2, R.layout.item_tenement_fee2) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, final TenementFeeBean model, final int position) {
                //GlideUtils.load(mActivity,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_desc, model.getTitle());
                holder.text(R.id.tv_money, model.getMoney());

                //查看详细
                holder.itemView.findViewById(R.id.tv_detail).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new XPopup.Builder(mContext)
                                .dismissOnTouchOutside(true)
                                .asCustom(new TenementDetailPop(mContext,model))
                                .show();
                    }
                });
            }
        });
        mAdpater2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(NotiDetailActivity.class).launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd1.clear();
        mddddd2.clear();
        mAdpater1.refresh(mddddd1);
        mAdpater2.refresh(mddddd2);
    }

    private void firstData(){
        Api.getApiService().getWuYeFei(SPUtils.getCustomId()).enqueue(new ZyCallBack<MyTenementBean>() {
            @Override
            public void doSuccess() {
                MyTenementBean bean = mData;
                tv_title.setText(bean.getTitle());
                if (TextUtils.isEmpty(bean.getTitle())){
                    ll_notice.setVisibility(View.GONE);
                } else {
                    ll_notice.setVisibility(View.VISIBLE);
                }
                mddddd1.clear();
                mddddd1.addAll(bean.getList());
                mAdpater1.refresh(mddddd1);
            }
        });
    }

    private void initData(){
        Api.getApiService().getWuYeFeiLiShi(SPUtils.getCustomId(),pageIndex+"").enqueue(new ZyCallBack<MyTenementBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyTenementBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    if (pageIndex ==1){
                        mddddd2.clear();
                    }
                    mddddd2.addAll(bean.getList());
                    mAdpater2.refresh(mddddd2);
                }
            }
        });
    }


}
