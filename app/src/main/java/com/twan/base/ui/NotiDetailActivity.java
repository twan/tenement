package com.twan.base.ui;

import com.twan.base.R;
import com.twan.base.app.BaseActivity;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class NotiDetailActivity extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.activity_noti_detail;
    }

    @Override
    protected void initEventAndData() {

    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("公告详情");
    }
}
