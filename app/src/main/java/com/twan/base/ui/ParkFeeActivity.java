package com.twan.base.ui;

import android.view.View;

import com.twan.base.R;
import com.twan.base.app.BaseActivity;
import com.twan.mylibary.router.Router;

import butterknife.OnClick;

/**
 * @author Twan
 * @date 2019/4/24
 *
 * 停车缴费
 */
public class ParkFeeActivity extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.activity_park_fee;
    }

    @Override
    protected void initEventAndData() {

    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("停车缴费");
        ib_more.setVisibility(View.VISIBLE);
        ib_more.setImageResource(R.mipmap.scan);
    }

    @OnClick({R.id.more})
    public void doOnclick(View view){
        switch (view.getId()){
            case R.id.more:
                Router.newIntent(ParkFeeActivity.this).to(ScanActivity.class).launch();
                break;
        }
    }
}
