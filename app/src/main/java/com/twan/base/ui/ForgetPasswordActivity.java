package com.twan.base.ui;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.twan.base.R;
import com.twan.base.api.Api;
import com.twan.base.api.Result;
import com.twan.base.app.BaseActivity;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.LogUtil;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;
import com.twan.mylibary.router.Router;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class ForgetPasswordActivity extends BaseActivity {
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_verfiy) EditText edt_verfiy;
    @BindView(R.id.btn_verfiy) Button btn_verfiy;
    @BindView(R.id.edt_pass) EditText edt_pass;
    @BindView(R.id.edt_pass2) EditText edt_pass2;
    @BindView(R.id.btn_reset) Button btn_reset;

    private CountDownTimer timer;

    @Override
    protected int getLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected void initEventAndData() {

    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("忘记密码");
    }

    @OnClick({R.id.btn_verfiy, R.id.btn_reset})
    public void doOnclick(View view) {
        switch (view.getId()) {
            case R.id.btn_verfiy:
                getVerfiy();
                break;
            case R.id.btn_reset:
                reset();
                break;
        }
    }

    private void getVerfiy() {
        String phone = edt_phone.getText().toString();
        if (!TextUtils.isEmpty(phone)) {
            phoneYzm(phone);
        } else {
            edt_phone.requestFocus();
            edt_phone.setError("请填写手机号");
            return;
        }
    }

    //发送验证码
    private void phoneYzm(String phone) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", phone);
        Api.getApiService().phoneYzm(phone).enqueue(new ZyCallBack<Void>() {
            @Override
            public void doSuccess() {
                timer();
                LogUtil.d("发送验证码成功");
                ToastUtil.shortShow("验证码发送成功,请注意查收");
            }
        });
    }

    private void timer() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                btn_verfiy.setClickable(false);
                btn_verfiy.setText(millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                btn_verfiy.setClickable(true);
                btn_verfiy.setText("获取验证码");
            }
        };
        timer.start();
    }

    private void reset() {
        String phone = edt_phone.getText().toString();
        String verfiy = edt_verfiy.getText().toString();
        String pass = edt_pass.getText().toString();
        String pass2 = edt_pass2.getText().toString();

        if (TextUtils.isEmpty(phone)) {
            edt_phone.requestFocus();
            edt_phone.setError("请填写手机号");
            return;
        }

        if (TextUtils.isEmpty(verfiy)) {
            edt_verfiy.requestFocus();
            edt_verfiy.setError("请填写验证码");
            return;
        }

        if (TextUtils.isEmpty(pass)) {
            edt_pass.requestFocus();
            edt_pass.setError("请填写密码");
            return;
        }

        if (TextUtils.isEmpty(pass2)) {
            edt_pass2.requestFocus();
            edt_pass2.setError("请再次填写密码");
            return;
        }

        if (!pass.equals(pass2)) {
            edt_pass2.requestFocus();
            edt_pass2.setError("两次密码不一致");
            return;
        }

        Api.getApiService().editPwd(phone,verfiy, pass).enqueue(new ZyCallBack<Void>() {
            @Override
            public void doSuccess() {
                LogUtil.d("密码重置成功");
                ForgetPasswordActivity.this.finish();
                ToastUtil.success("密码重置成功");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }
}
