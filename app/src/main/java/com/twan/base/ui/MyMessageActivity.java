package com.twan.base.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.twan.base.R;
import com.twan.base.adapter.MessagePagerAdapter;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.LoginBean;
import com.twan.base.entity.UnReadBean;
import com.twan.base.fragment.MessageLeftPresenter;
import com.twan.base.fragment.MessageRightPresenter;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;
import com.twan.base.widget.ScaleTransitionPagerTitleView;
import com.twan.mylibary.router.Router;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.twan.base.app.Constants.SP_CUSTOM_ID;
import static com.twan.base.app.Constants.SP_LOGIN_BEAN;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class MyMessageActivity extends BaseActivity {

    @BindView(R.id.pager) ViewPager mViewPager;
    List<View> viewContainter;
    private MessagePagerAdapter mPagerAdapter;
    private MagicIndicator magicIndicator;

    private static String[] CHANNELS = new String[]{"通知消息", "平台消息"};
    private List<String> mDataList = Arrays.asList(CHANNELS);

    @Override
    protected int getLayout() {
        return R.layout.activity_my_message;
    }

    @Override
    protected void initEventAndData() {
        LayoutInflater lf = LayoutInflater.from(mContext);
        View view1 = lf.inflate(R.layout.fragment_message_left, null);
        View view2 = lf.inflate(R.layout.fragment_message_right, null);
        viewContainter =new ArrayList<View>();
        viewContainter .add(view1);
        viewContainter .add(view2);

        mPagerAdapter = new MessagePagerAdapter(viewContainter);
        mViewPager.setAdapter(mPagerAdapter);

        new MessageLeftPresenter(view1,this);
        new MessageRightPresenter(view2,this);
        initMagicIndicator();
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("消息");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUnReadNum();
    }

    private void initMagicIndicator() {
        magicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator);
        magicIndicator.setBackgroundColor(Color.WHITE);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setTextSize(16);
                simplePagerTitleView.setNormalColor(Color.parseColor("#616161"));
                simplePagerTitleView.setSelectedColor(getColor(R.color.app_blue));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(1.6f));
                indicator.setLineHeight(UIUtil.dip2px(context, 1));
                indicator.setColors(getColor(R.color.app_blue));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    private void getUnReadNum(){
        Api.getApiService().getNum(SPUtils.getCustomId()).enqueue(new ZyCallBack<UnReadBean>() {
            @Override
            public void doSuccess() {
                UnReadBean bean = mData;
                if (bean != null){
                    String tongzhi="",pingtai="";
                    if (Integer.valueOf(bean.getNum1())>0){
                        tongzhi = "通知消息("+bean.getNum1()+")";
                    } else {
                        tongzhi = "通知消息";
                    }

                    if (Integer.valueOf(bean.getNum2())>0){
                        pingtai = "平台消息("+bean.getNum2()+")";
                    } else {
                        pingtai = "平台消息";
                    }

                    CHANNELS = new String[]{tongzhi, pingtai};
                    mDataList = Arrays.asList(CHANNELS);
                    initMagicIndicator();
                }
            }
        });
    }
}
