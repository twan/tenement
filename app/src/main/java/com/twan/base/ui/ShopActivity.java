package com.twan.base.ui;

import com.twan.base.R;
import com.twan.base.app.BaseActivity;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class ShopActivity extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.activity_shop;
    }

    @Override
    protected void initEventAndData() {

    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("商城");
    }
}
