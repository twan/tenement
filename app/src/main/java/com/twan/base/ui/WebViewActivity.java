package com.twan.base.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import com.twan.base.R;
import com.twan.base.app.BaseActivity;
import com.twan.base.util.MyUtil;
import com.twan.base.widget.ProgressWebView;

import butterknife.BindView;
import butterknife.OnClick;

public class WebViewActivity extends BaseActivity {

    @BindView(R.id.webView)
    ProgressWebView mWeb;
    private String url, titlestr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_webview;
    }

    @Override
    protected void initEventAndData() {
        initTitle();
        MyUtil.setWebView(mWeb).loadUrl(url);
    }

    @Override
    protected void initTitle() {
        url = getIntent().getStringExtra("url");
        titlestr = getIntent().getStringExtra("title");

        if (TextUtils.isEmpty(titlestr)) {
            titlestr = "空网页";
        }
        title.setText(titlestr);
    }

    @OnClick({R.id.back, R.id.tv_webview_close})
    public void back(View view) {
        switch (view.getId()) {
            case R.id.tv_webview_close:
                this.finish();
                break;
            case R.id.back:
                if (mWeb.canGoBack()) {
                    mWeb.goBack();
                } else {
                    this.finish();
                }
                break;
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && mWeb.canGoBack()) {
            mWeb.goBack();
            return true;
        } else {
            this.finish();
        }

        return super.onKeyDown(keyCode, event);
    }
}
