package com.twan.base.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.twan.base.R;
import com.twan.base.adapter.MessagePagerAdapter;
import com.twan.base.app.BaseActivity;
import com.twan.base.fragment.FeeLeftPresenter;
import com.twan.base.fragment.FeeRightPresenter;
import com.twan.base.fragment.MessageLeftPresenter;
import com.twan.base.fragment.MessageRightPresenter;
import com.twan.mylibary.router.Router;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class MyFeeActivity extends BaseActivity {

    @BindView(R.id.pager) ViewPager mViewPager;
    List<View> viewContainter;
    private MessagePagerAdapter mPagerAdapter;

    private static final String[] CHANNELS = new String[]{"物业费记录", "水费记录"};
    private List<String> mDataList = Arrays.asList(CHANNELS);

    @Override
    protected int getLayout() {
        return R.layout.activity_my_fee;
    }

    @Override
    protected void initEventAndData() {
        LayoutInflater lf = LayoutInflater.from(mContext);
        View view1 = lf.inflate(R.layout.fragment_fee_left, null);
        View view2 = lf.inflate(R.layout.fragment_fee_right, null);
        viewContainter =new ArrayList<View>();
        viewContainter .add(view1);
        viewContainter .add(view2);

        mPagerAdapter = new MessagePagerAdapter(viewContainter);
        mViewPager.setAdapter(mPagerAdapter);

        new FeeLeftPresenter(view1,this);
        new FeeRightPresenter(view2,this);
        initMagicIndicator();
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("我的缴费记录");
    }

    private void initMagicIndicator() {
        MagicIndicator magicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator);
        magicIndicator.setBackgroundColor(Color.WHITE);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setTextSize(16);
                simplePagerTitleView.setNormalColor(Color.parseColor("#616161"));
                simplePagerTitleView.setSelectedColor(getColor(R.color.app_blue));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(1.6f));
                indicator.setLineHeight(UIUtil.dip2px(context, 1));
                indicator.setColors(getColor(R.color.app_blue));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }
}
