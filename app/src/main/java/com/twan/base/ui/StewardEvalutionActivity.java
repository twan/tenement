package com.twan.base.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.XPopupImageLoader;
import com.twan.base.R;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.BaoxiuDetailBean;
import com.twan.base.entity.MyTenementBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.GlideUtils;
import com.twan.base.util.SPUtils;
import com.twan.base.widget.EvalutionCenterPop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Twan
 * @date 2019/4/24
 * 评价
 */
public class StewardEvalutionActivity extends BaseActivity {
    @BindView(R.id.tv_type) TextView tv_type;
    @BindView(R.id.tv_desc) TextView tv_desc;
    @BindView(R.id.tv_name) TextView tv_name;
    @BindView(R.id.tv_phone) TextView tv_phone;
    @BindView(R.id.tv_hao) TextView tv_hao;
    @BindView(R.id.btn_evalution) TextView btn_evalution;

    @BindView(R.id.iv_1) ImageView iv_1;
    @BindView(R.id.iv_2) ImageView iv_2;
    @BindView(R.id.iv_3) ImageView iv_3;

    String mId="",pjzt="",zt="";
    List<String> imgUrls = new ArrayList<>();
    @Override
    protected int getLayout() {
        return R.layout.activity_steward_evalution;
    }

    @Override
    protected void initEventAndData() {
        mId = getIntent().getStringExtra("id");
        pjzt = getIntent().getStringExtra("pjzt");
        zt = getIntent().getStringExtra("zt");
        initData(mId);

        if (!TextUtils.isEmpty(pjzt) && pjzt.equals("1") && zt.equals("2")){
            btn_evalution.setVisibility(View.VISIBLE);
        } else {
            btn_evalution.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("业主报修");
    }

    @OnClick({R.id.btn_evalution,R.id.iv_1,R.id.iv_2,R.id.iv_3})
    public void doOnclick(View view){
        switch (view.getId()){
            case R.id.btn_evalution:
                new XPopup.Builder(mContext)
                        .dismissOnTouchOutside(true)
                        .asCustom(new EvalutionCenterPop(mContext, mId, new EvalutionCenterPop.IEvalution() {
                            @Override
                            public void onsucess() {
                                btn_evalution.setVisibility(View.GONE);
                            }
                        }))
                        .show();
                break;
            case R.id.iv_1:
                new XPopup.Builder(StewardEvalutionActivity.this)
                        .asImageViewer(iv_1, imgUrls.get(0), new ImageLoader())
                        .show();
                break;
            case R.id.iv_2:
                new XPopup.Builder(StewardEvalutionActivity.this)
                        .asImageViewer(iv_2, imgUrls.get(1), new ImageLoader())
                        .show();
                break;
            case R.id.iv_3:
                new XPopup.Builder(StewardEvalutionActivity.this)
                        .asImageViewer(iv_3, imgUrls.get(2), new ImageLoader())
                        .show();
                break;
        }
    }

    private void initData(String id) {
        Api.getApiService().baoXiuDetails(SPUtils.getCustomId(),id).enqueue(new ZyCallBack<BaoxiuDetailBean>() {
            @Override
            public void doSuccess() {
                BaoxiuDetailBean bean = mData;
                if (bean != null){
                    tv_type.setText(bean.getType().equals("1")?"室内报修":"室外报修");
                    tv_desc.setText(bean.getDescribe());
                    tv_name.setText(bean.getName());
                    tv_phone.setText(bean.getPhone());
                    tv_hao.setText(bean.getHao());

                    parseImg(bean.getImgList());
                }
            }
        });
    }

    private void parseImg(List<String> list){
        imgUrls.clear();
        imgUrls.addAll(list);

        iv_1.setVisibility(View.GONE);
        iv_2.setVisibility(View.GONE);
        iv_3.setVisibility(View.GONE);
        if (list != null && list.size()>0){
            iv_1.setVisibility(View.VISIBLE);
            GlideUtils.load(this,iv_1,list.get(0));
        }
        if (list != null && list.size()>1) {
            iv_2.setVisibility(View.VISIBLE);
            GlideUtils.load(this,iv_2,list.get(1));
        };
        if (list != null && list.size()>2) {
            iv_3.setVisibility(View.VISIBLE);
            GlideUtils.load(this,iv_3,list.get(2));
        }
    }

    //加载大图
    class ImageLoader implements XPopupImageLoader {
        @Override
        public void loadImage(int position, @NonNull Object url, @NonNull ImageView imageView) {
            //必须指定Target.SIZE_ORIGINAL，否则无法拿到原图，就无法享用天衣无缝的动画
            Glide.with(imageView).load(url).apply(new RequestOptions().override(Target.SIZE_ORIGINAL)).into(imageView);
        }

        @Override
        public File getImageFile(@NonNull Context context, @NonNull Object uri) {
            try {
                return Glide.with(context).downloadOnly().load(uri).submit().get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
