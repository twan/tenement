package com.twan.base.ui;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.twan.base.R;
import com.twan.base.adapter.BaseRecyclerAdapter;
import com.twan.base.adapter.SmartViewHolder;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.ConvenServiceBean;
import com.twan.base.entity.ConvenServiceType;
import com.twan.base.entity.HouseNotiBean;
import com.twan.base.entity.MyNoticeBean;
import com.twan.base.entity.MyShopBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.GlideUtils;
import com.twan.base.util.SPUtils;
import com.twan.mylibary.router.Router;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class ConvenServiceActivity extends BaseActivity {
    @BindView(R.id.refreshLayout) SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.tv_type) TextView tv_type;

    private int pageIndex =1;
    List<ConvenServiceBean> mddddd = new ArrayList<>();
    BaseRecyclerAdapter<ConvenServiceBean> mAdpater;
    List<ConvenServiceType.LxType> mTypes = new ArrayList<>();
    String lxid="1";

    @Override
    protected int getLayout() {
        return R.layout.activity_conven_service;
    }

    @Override
    protected void initEventAndData() {
        getCServiceType();
        initRefresh();
        initData();
    }

    @Override
    protected void initTitle() {
        super.initTitle();
        title.setText("便民服务");
    }

    @OnClick({R.id.rl_select})
    public void doOnclick(View view){
        switch (view.getId()){
            case R.id.rl_select:
                if (mTypes.size() == 0){
                    return ;
                }
                ArrayList<String> tystr = new ArrayList<>();
                String[] tyarr = new String[]{};
                for (ConvenServiceType.LxType lxType:mTypes){
                    tystr.add(lxType.getLxName());
                }

                new XPopup.Builder(ConvenServiceActivity.this)
                        .atView(view)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList(tystr.toArray(tyarr),
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        lxid = mTypes.get(position).getLxId();
                                        tv_type.setText("类别: "+mTypes.get(position).getLxName());
                                        initData();
                                    }
                                })
                        .show();
                break;
        }
    }

    private void initRefresh() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(mContext));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(mContext));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdpater = new BaseRecyclerAdapter<ConvenServiceBean>(mddddd, R.layout.item_conven_service) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, ConvenServiceBean model, final int position) {
                GlideUtils.load(ConvenServiceActivity.this,(ImageView) holder.itemView.findViewById(R.id.iv_head),model.getImg());
                holder.text(R.id.tv_desc, model.getShopName());
                holder.text(R.id.tv_phone, Html.fromHtml("商家电话: <font color='#FF6A6A'>"+model.getPhone()+"</font>"));
                holder.text(R.id.tv_addr, Html.fromHtml("商家地址: <font color='#778899' size=10>"+model.getAddress()+"</font>"));
            }
        });
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                dataClear();
                initData();
            }
        });
        mRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                pageIndex++;
                initData();
            }
        });
        mAdpater.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Router.newIntent(mActivity).to(NotiDetailActivity.class).launch();
                Router.newIntent(ConvenServiceActivity.this).to(WebViewActivity.class)
                        .putString("title","便民服务")
                        .putString("url",mddddd.get(position).getUrl())
                        .launch();
            }
        });
    }

    private void dataClear(){
        pageIndex=1;
        mddddd.clear();
        mAdpater.refresh(mddddd);
    }

    protected void initData() {
        Api.getApiService().getShop(SPUtils.getCustomId(),lxid,pageIndex + "").enqueue(new ZyCallBack<MyShopBean>(mRefreshLayout) {
            @Override
            public void doSuccess() {
                MyShopBean bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    if (pageIndex ==1){
                        dataClear();
                    }
                    mddddd.addAll(bean.getList());
                    mAdpater.refresh(mddddd);
                }
            }
        });
    }

    //便民服务类别
    private void getCServiceType(){
        Api.getApiService().getLieBie().enqueue(new ZyCallBack<ConvenServiceType>() {
            @Override
            public void doSuccess() {
                ConvenServiceType bean = mData;
                if (bean.getList() != null && bean.getList().size() > 0) {
                    mTypes.clear();
                    mTypes.addAll(bean.getList());
                }
            }
        });
    }
}
