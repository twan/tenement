package com.twan.base.ui;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.jaeger.library.StatusBarUtil;
import com.twan.base.R;
import com.twan.base.api.Api;
import com.twan.base.app.BaseActivity;
import com.twan.base.entity.LoginBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.util.GlideUtils;
import com.twan.base.util.JsonUtil;
import com.twan.base.util.LogUtil;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;
import com.twan.mylibary.router.Router;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;

import static com.twan.base.app.Constants.SP_CUSTOM_ID;
import static com.twan.base.app.Constants.SP_LOGIN_BEAN;

/**
 * @author Twan
 * @date 2019/4/24
 */
public class LoginActivity extends BaseActivity {
    @BindView(R.id.edt_pass) EditText edt_pass;
    @BindView(R.id.tv_phone) TextView tv_phone;
    @BindView(R.id.chb_protocol) CheckBox chb_protocol;

    @Override
    protected int getLayout() {
        return R.layout.activity_mobile_login;
    }

//    @Override
//    protected void setStatusBar() {
//        StatusBarUtil.setTranslucentForImageView(this, 0,null);
//    }


    @Override
    protected void initEventAndData() {
        setSwipeBackEnable(false);
        if (!TextUtils.isEmpty(SPUtils.getCustomId())){
            Router.newIntent(LoginActivity.this).to(MainActivity.class).launch();
            this.finish();
        }
    }

    @OnClick({R.id.tv_forget_password,R.id.btn_login,R.id.tv_protocol})
    public void doClick(View view){
        switch (view.getId()){
            case R.id.tv_forget_password:
                Router.newIntent(LoginActivity.this).to(ForgetPasswordActivity.class).launch();
                break;
            case R.id.btn_login:
                login();
                break;
            case R.id.tv_protocol:
                Router.newIntent(LoginActivity.this).to(WebViewActivity.class)
                        .putString("title","服务协议")
                        .putString("url","https://app.anjiwuye.com/json/content.aspx?id=1")
                        .launch();
                break;
        }
    }

    private void login(){
        String pass = edt_pass.getText().toString();
        String phone = tv_phone.getText().toString();
        if (TextUtils.isEmpty(pass)){
            edt_pass.requestFocus();
            edt_pass.setError("请填写密码");
            return;
        }

        if (TextUtils.isEmpty(phone)){
            tv_phone.requestFocus();
            tv_phone.setError("请填写手机号");
            return;
        }

        if (!chb_protocol.isChecked()){
            ToastUtil.warn("请勾选服务协议!");
            return;
        }
        String rid = JPushInterface.getRegistrationID(getApplicationContext());
        JPushInterface.setAlias(getApplicationContext(),1,rid);//phone
        LogUtil.d("极光rid="+rid);
        Api.getApiService().login(phone,pass,rid).enqueue(new ZyCallBack<LoginBean>() {
            @Override
            public void doSuccess() {
                LoginBean bean = mData;
                if (bean != null){
                    ToastUtil.success("登录成功!");
                    SPUtils.getInstance().put(SP_CUSTOM_ID,bean.getCustomId());
                    SPUtils.getInstance().put(SP_LOGIN_BEAN, JsonUtil.objectToJson(bean));
                    Router.newIntent(LoginActivity.this).to(MainActivity.class).launch();
                    LoginActivity.this.finish();
                }
            }
        });
    }
}
