package com.twan.base.api;

/**
 * Created by twan on 2017/11/9.
 */

public class Result<T> {
    public int result;
    public String msg;
    public T data;
    public long count;
    public long page;

    @Override
    public String toString() {
        return "Result{" +
                "result=" + result +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", count=" + count +
                ", page=" + page +
                '}';
    }
}
