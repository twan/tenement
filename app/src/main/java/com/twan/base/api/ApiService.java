package com.twan.base.api;

import com.twan.base.entity.AdertisBean;
import com.twan.base.entity.AliPayBean;
import com.twan.base.entity.BaoxiuDetailBean;
import com.twan.base.entity.Bean;
import com.twan.base.entity.ConnectBean;
import com.twan.base.entity.ConvenServiceType;
import com.twan.base.entity.FeeBean;
import com.twan.base.entity.Good;
import com.twan.base.entity.JianKongBean;
import com.twan.base.entity.LoginBean;
import com.twan.base.entity.MyFamilyBean;
import com.twan.base.entity.MyFee;
import com.twan.base.entity.MyIndexBean;
import com.twan.base.entity.MyMessage;
import com.twan.base.entity.MyNoticeBean;
import com.twan.base.entity.MyShopBean;
import com.twan.base.entity.MyStewardBean;
import com.twan.base.entity.MyTenementBean;
import com.twan.base.entity.MyWaterBean;
import com.twan.base.entity.RelationBean;
import com.twan.base.entity.UnReadBean;
import com.twan.base.entity.UploadImgBean;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by twan on 2017/11/9.
 */

public interface ApiService {

    //用户登录接口
    @POST("json/login.aspx")
    Call<Result<LoginBean>> login(@Query("phone") String phone,
                                  @Query("password") String password,
                                  @Query("tsId") String tsId);

    //获取验证码
    @POST("json/phoneYzm.aspx")
    Call<Result> phoneYzm(@Query("phone") String phone);

    //重置密码
    @POST("json/editPwd.aspx")
    Call<Result> editPwd(@Query("phone") String phone,@Query("yzm") String yzm, @Query("password") String password);

    //我的家人
    @POST("json/myJia.aspx")
    Call<Result<MyFamilyBean>> myJia(@Query("customId") String customId, @Query("page") String page);

    //添加家人-获取家人手机验证码
    @POST("json/jiaYzm.aspx")
    Call<Result> jiaYzm(@Query("phone") String phone);

    //添加家人
    @POST("json/addJia.aspx")
    Call<Result> addJia(@Query("customId") String customId,
                        @Query("gxId") String gxId,
                        @Query("phone") String phone,
                        @Query("yzm") String yzm,
                        @Query("password") String password);

    //添加家人-关系选择数据
    @POST("json/getGx.aspx")
    Call<Result<RelationBean>> getGx();

    //我的-物业费记录
    @POST("json/getWuYeFeiJiLu.aspx")
    Call<Result<MyFee>> getWuYeFeiJiLu(@Query("customId") String customId, @Query("page") String page);

    //我的-水费记录
    @POST("json/getShuiFeiJiLu.aspx")
    Call<Result<MyFee>> getShuiFeiJiLu(@Query("customId") String customId, @Query("page") String page);

    //首页轮播图和最新公告
    @POST("json/getIndex.aspx")
    Call<Result<MyIndexBean>> getIndex(@Query("customId") String customId);

    //首页广告列表
    @POST("json/getGg.aspx")
    Call<Result<AdertisBean>> getGg(@Query("customId") String customId, @Query("page") String page);

    //我的消息-未读消息数
    @POST("json/getNum.aspx")
    Call<Result<UnReadBean>> getNum(@Query("customId") String customId);

    //我的消息列表
    @POST("json/getMess.aspx")
    Call<Result<MyMessage>> getMess(@Query("customId") String customId, @Query("type") String type,@Query("page") String page);

    //消息状态变更已读
    @POST("json/editMess.aspx")
    Call<Result> editMess(@Query("customId") String customId, @Query("id") String id);

    //小区公告
    @POST("json/getGongGao.aspx")
    Call<Result<MyNoticeBean>> getGongGao(@Query("customId") String customId, @Query("page") String page);

    //商家列表
    @POST("json/getShop.aspx")
    Call<Result<MyShopBean>> getShop(@Query("customId") String customId, @Query("lxId") String lxId, @Query("page") String page);

    //物业缴费-逾期提醒和应缴账单
    @POST("json/getWuYeFei.aspx")
    Call<Result<MyTenementBean>> getWuYeFei(@Query("customId") String customId);

    //物业缴费-历史账单
    @POST("json/getWuYeFeiLiShi.aspx")
    Call<Result<MyTenementBean>> getWuYeFeiLiShi(@Query("customId") String customId,@Query("page") String page);

    //水费代缴-逾期提醒和应缴账单
    @POST("json/getShuiFei.aspx")
    Call<Result<MyWaterBean>> getShuiFei(@Query("customId") String customId);

    //物业缴费-历史账单
    @POST("json/getShuiFeiLiShi.aspx")
    Call<Result<MyWaterBean>> getShuiFeiLiShi(@Query("customId") String customId,@Query("page") String page);

    //业主报修
    @POST("json/addBx.aspx")
    Call<Result> addBx(@Query("customId") String customId,
                       @Query("type") String type,
                       @Query("describe") String describe,
                       @Query("imgList") String imgList,
                       @Query("name") String name,
                       @Query("phone") String phone,
                       @Query("hao") String hao);

    //图片上传
    @Multipart
    @POST("json/addImg.aspx")
    Call<Result<UploadImgBean>> addImg(@Part("img") RequestBody base64);

    //报修列表
    @POST("json/getBaoXiu.aspx")
    Call<Result<MyStewardBean>> getBaoXiu(@Query("customId") String customId, @Query("page") String page);

    //报修详情
    @POST("json/baoXiuDetails.aspx")
    Call<Result<BaoxiuDetailBean>> baoXiuDetails(@Query("customId") String customId, @Query("id") String id);

    //报修评价
    @POST("json/addPj.aspx")
    Call<Result> addPj(@Query("customId") String customId,
                       @Query("id") String id,
                       @Query("pjDj") String pjDj,
                       @Query("memo") String memo);

    //报修撤销
    @POST("json/delBaoXiu.aspx")
    Call<Result> delBaoXiu(@Query("customId") String customId, @Query("id") String id);

    //便民服务类别
    @POST("json/getLieBie.aspx")
    Call<Result<ConvenServiceType>> getLieBie();

    //支付
    @POST("json/addZf.aspx")
    Call<Result<AliPayBean>> addZf(@Query("type") String type, @Query("id") String id, @Query("zffs") String zffs);

    //查看监控列表
    @POST("json/getJianKong.aspx")
    Call<Result<JianKongBean>> getJianKong(@Query("customId") String customId);

    //根据设备编号获取连接信息
    @POST("json/sbDetail.aspx")
    Call<Result<ConnectBean>> sbDetail(@Query("customId") String customId, @Query("sbId") String sbId);
}
