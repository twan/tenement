package com.twan.base.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.twan.base.widget.GlideRoundTransform;
import com.twan.base.widget.RoundCorner;

import java.util.concurrent.ExecutionException;

public class GlideUtils {

    public static void load(final Context context, final ImageView view, String url) {
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(view);
    }

    //把图片用圆形展示
    public static void loadCicle(final Context context, final ImageView view, String url) {
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new GlideRoundTransform(context)).into(view);
    }

    //加载普通圆角图片,四个角都有弧形
    //https://github.com/JadynAi/Kotlin-D/blob/master/app/src/main/java/com/jadynai/kotlindiary/img/RoundCorner.kt
    public static void loadRoundCorner(final Context context, final ImageView view, String url) {
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .apply(RequestOptions.bitmapTransform(new RoundCorner(20f)))
                .into(view);
    }

    //顶部2个角弧形
    public static void loadRoundCorner2(final Context context, final ImageView view, String url) {
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .apply(RequestOptions.bitmapTransform(new RoundCorner(20f, 20f,0,0f)))
                .into(view);
    }

    public static void load(final Context context, final ImageView view, String url,@DrawableRes int placeholder) {
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(placeholder).into(view);
    }

    public static Bitmap loadBitmap(final Context context, String url) {
        try {
            return Glide.with(context).asBitmap().load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
