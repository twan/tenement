package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/25
 */
public class HouseNotiBean implements Serializable {
    private String newsId;//": "1",
    private String title;//": "物业缴费通知",
    private String img;//": "http://wuye.gugangkf.cn/Upload/1.jpg",
    private String zuozhe;//": "物业管理处",
    private String pudate;//": "2019-01-05 15:47:25",
    private String url;//": "http://wuye.gugangkf.cn/json/news.aspx?id=1"

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getZuozhe() {
        return zuozhe;
    }

    public void setZuozhe(String zuozhe) {
        this.zuozhe = zuozhe;
    }

    public String getPudate() {
        return pudate;
    }

    public void setPudate(String pudate) {
        this.pudate = pudate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
