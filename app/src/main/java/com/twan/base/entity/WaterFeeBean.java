package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/25
 */
public class WaterFeeBean implements Serializable {
    private String id;//": "2",
    private String title;//": "2019年3月-2019年6月份账单",
    private String money;//": "529.2",
    private String biao1;//": "126",
    private String biao2;//": "156",
    private String zhouqi;//": "2019.03-2019.06",
    private String danjia;//": "3元",
    private String sum;//": "（156-126）*3=90元"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getBiao1() {
        return biao1;
    }

    public void setBiao1(String biao1) {
        this.biao1 = biao1;
    }

    public String getBiao2() {
        return biao2;
    }

    public void setBiao2(String biao2) {
        this.biao2 = biao2;
    }

    public String getZhouqi() {
        return zhouqi;
    }

    public void setZhouqi(String zhouqi) {
        this.zhouqi = zhouqi;
    }

    public String getDanjia() {
        return danjia;
    }

    public void setDanjia(String danjia) {
        this.danjia = danjia;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }
}
