package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class MyTenementBean extends BaseBean {

    private String title;
    private List<TenementFeeBean> list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TenementFeeBean> getList() {
        return list;
    }

    public void setList(List<TenementFeeBean> list) {
        this.list = list;
    }
}
