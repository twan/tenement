package com.twan.base.entity;


import java.util.List;

/**
 * @author Twan
 * @date 2019/5/8
 */
public class UnReadBean extends BaseBean {

    private String num1;//": "1",
    private String num2;//": "2"

    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }
}
