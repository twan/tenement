package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/9
 */
public class NotiMessage implements Serializable {
    private String result;
    private Receive data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Receive getData() {
        return data;
    }

    public void setData(Receive data) {
        this.data = data;
    }

    public static class Receive implements Serializable{
        private String title;
        private String img;
        private String url;
        private String desc;
        private String id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
