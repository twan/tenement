package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/25
 */
public class ConvenServiceBean implements Serializable {
    private String shopId;//": "1",
    private String shopName;//": "新时代烤肉",
    private String img;//": "http://wuye.gugangkf.cn/Upload/1.jpg",
    private String phone;//": "18144585551",
    private String address;//": "上海市闸北区****",
    private String url;//": "http://wuye.gugangkf.cn/json/shop.aspx?id=1"

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
