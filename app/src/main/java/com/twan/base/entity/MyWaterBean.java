package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class MyWaterBean extends BaseBean {

    private String title;
    private List<WaterFeeBean> list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<WaterFeeBean> getList() {
        return list;
    }

    public void setList(List<WaterFeeBean> list) {
        this.list = list;
    }
}
