package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/10
 */
public class UploadImgBean extends BaseBean {
    private String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
