package com.twan.base.entity;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class ConnectBean extends BaseBean {

    private String sbName;//": "东大门",
    private String szIp;//": "333.185.245.111",
    private String nPort;//": "9000",
    private String szUsername;//": "admin",
    private String szPassword;//": "admin666",
    private String szCameraId;//": "1000001$1$0$9";

    public String getSbName() {
        return sbName;
    }

    public void setSbName(String sbName) {
        this.sbName = sbName;
    }

    public String getSzIp() {
        return szIp;
    }

    public void setSzIp(String szIp) {
        this.szIp = szIp;
    }

    public String getnPort() {
        return nPort;
    }

    public void setnPort(String nPort) {
        this.nPort = nPort;
    }

    public String getSzUsername() {
        return szUsername;
    }

    public void setSzUsername(String szUsername) {
        this.szUsername = szUsername;
    }

    public String getSzPassword() {
        return szPassword;
    }

    public void setSzPassword(String szPassword) {
        this.szPassword = szPassword;
    }

    public String getSzCameraId() {
        return szCameraId;
    }

    public void setSzCameraId(String szCameraId) {
        this.szCameraId = szCameraId;
    }
}
