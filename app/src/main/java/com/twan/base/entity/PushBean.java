package com.twan.base.entity;

/**
 * Created by Administrator on 2018/3/6.
 */

public class PushBean extends BaseBean {
    //{"apply_no":"114809","msg":"您提报的申请编号:[114809],客户姓名:[冯宇],当前已经到[总部风控审批中]节点","phone":"13764204128","type":"0"}\

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class MyBean{
        private String apply_no;
        private String msg;
        private String phone;
        private String type;


        public String getApply_no() {
            return apply_no;
        }

        public void setApply_no(String apply_no) {
            this.apply_no = apply_no;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
