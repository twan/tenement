package com.twan.base.entity;

import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/8
 */
public class MyIndexBean extends BaseBean {

    private List<Banner> list;
    private String title;

    public List<Banner> getList() {
        return list;
    }

    public void setList(List<Banner> list) {
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static class Banner extends SimpleBannerInfo {
        private String img;//": "http://wuye.gugangkf.cn/Upload/1.jpg",
        private String url;//": "https://www.baidu.com/"

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String getXBannerUrl() {
            return img;
        }
    }

}
