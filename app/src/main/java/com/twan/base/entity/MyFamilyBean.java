package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/6
 */
public class MyFamilyBean extends BaseBean {

    private List<Member> list;
    private MyPage extra;

    public List<Member> getList() {
        return list;
    }

    public void setList(List<Member> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }

    public static class Member{
        private String jiaId;//": "1",
        private String gxId;//": "1",
        private String gxName;//": "父亲",
        private String phone;//": "18133452581",
        private String zt;//": "1"

        public String getJiaId() {
            return jiaId;
        }

        public void setJiaId(String jiaId) {
            this.jiaId = jiaId;
        }

        public String getGxId() {
            return gxId;
        }

        public void setGxId(String gxId) {
            this.gxId = gxId;
        }

        public String getGxName() {
            return gxName;
        }

        public void setGxName(String gxName) {
            this.gxName = gxName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getZt() {
            return zt;
        }

        public void setZt(String zt) {
            this.zt = zt;
        }
    }
}
