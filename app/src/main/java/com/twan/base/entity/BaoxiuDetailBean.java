package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/10
 */
public class BaoxiuDetailBean extends BaseBean {
    private String pudate;//": "2018-07-15",
    private String zt;//": "1",
    private String pjZt;//": "1",
    private String describe;//": "我家的房顶漏水",
    private String type;//": "1",
    private String name;//": "萧炎",
    private String phone;//": "18111110000",
    private String hao;//": "8栋101",
    private List<String> imgList;//": ["http://wuye.gugangkf.cn/Upload

    public String getPudate() {
        return pudate;
    }

    public void setPudate(String pudate) {
        this.pudate = pudate;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

    public String getPjZt() {
        return pjZt;
    }

    public void setPjZt(String pjZt) {
        this.pjZt = pjZt;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHao() {
        return hao;
    }

    public void setHao(String hao) {
        this.hao = hao;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }
}
