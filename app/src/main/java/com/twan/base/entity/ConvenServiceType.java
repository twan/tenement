package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/20
 */
public class ConvenServiceType extends BaseBean {
    private List<LxType> list;

    public List<LxType> getList() {
        return list;
    }

    public void setList(List<LxType> list) {
        this.list = list;
    }

    public static class LxType{
        private String lxId;//": "1",
        private String lxName;//": "家政服务"

        public String getLxId() {
            return lxId;
        }

        public void setLxId(String lxId) {
            this.lxId = lxId;
        }

        public String getLxName() {
            return lxName;
        }

        public void setLxName(String lxName) {
            this.lxName = lxName;
        }
    }
}
