package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/8
 */
public class MyFee extends BaseBean {

    private List<FeeBean> list;
    private MyPage extra;

    public List<FeeBean> getList() {
        return list;
    }

    public void setList(List<FeeBean> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }
}
