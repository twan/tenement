package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/25
 */
public class StewardBean implements Serializable {
    private String id;//": "1",
    private String pudate;//": "2018-07-15",
    private String zt;//": "1",
    private String pjZt;//": "1",
    private String describe;//": "我家的房顶漏水"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPudate() {
        return pudate;
    }

    public void setPudate(String pudate) {
        this.pudate = pudate;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

    public String getPjZt() {
        return pjZt;
    }

    public void setPjZt(String pjZt) {
        this.pjZt = pjZt;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
