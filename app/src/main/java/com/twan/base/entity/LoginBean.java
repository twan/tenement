package com.twan.base.entity;

/**
 * @author Twan
 * @date 2019/5/6
 */
public class LoginBean extends BaseBean{
    private String customId;//": "1",
    private String name;//": "小刚",
    private String phone;//": "1436864169",
    private String address;//": "8号楼3单元606",
    private String size;//": "165",
    private String kefu;//": "18100150210",
    private String xiaoqu;//": "天安别墅"

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getKefu() {
        return kefu;
    }

    public void setKefu(String kefu) {
        this.kefu = kefu;
    }

    public String getXiaoqu() {
        return xiaoqu;
    }

    public void setXiaoqu(String xiaoqu) {
        this.xiaoqu = xiaoqu;
    }
}
