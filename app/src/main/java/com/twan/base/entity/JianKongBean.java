package com.twan.base.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class JianKongBean extends BaseBean {

    private ArrayList<JianKong> list;

    public ArrayList<JianKong> getList() {
        return list;
    }

    public void setList(ArrayList<JianKong> list) {
        this.list = list;
    }

    public static class JianKong implements Serializable{
        private String typeId;//
        private String typeName;//": "东门出口",
        private ArrayList<Shebei> shebei;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public ArrayList<Shebei> getShebei() {
            return shebei;
        }

        public void setShebei(ArrayList<Shebei> shebei) {
            this.shebei = shebei;
        }
    }

    public static class Shebei implements Serializable {
        private String sbId;
        private String sbName;//": "大桥向东"

        public String getSbId() {
            return sbId;
        }

        public void setSbId(String sbId) {
            this.sbId = sbId;
        }

        public String getSbName() {
            return sbName;
        }

        public void setSbName(String sbName) {
            this.sbName = sbName;
        }
    }
}
