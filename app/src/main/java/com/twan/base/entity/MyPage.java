package com.twan.base.entity;

/**
 * @author Twan
 * @date 2019/5/6
 */
public class MyPage extends BaseBean {
    private String page_size;//": "15",
    private String all_count;//": "0",
    private String all_page;//": "0",
    private String current_page;//": "1"

    public String getPage_size() {
        return page_size;
    }

    public void setPage_size(String page_size) {
        this.page_size = page_size;
    }

    public String getAll_count() {
        return all_count;
    }

    public void setAll_count(String all_count) {
        this.all_count = all_count;
    }

    public String getAll_page() {
        return all_page;
    }

    public void setAll_page(String all_page) {
        this.all_page = all_page;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }
}
