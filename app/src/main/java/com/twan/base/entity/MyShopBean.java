package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class MyShopBean extends BaseBean {
    private List<ConvenServiceBean> list;
    private MyPage extra;

    public List<ConvenServiceBean> getList() {
        return list;
    }

    public void setList(List<ConvenServiceBean> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }
}
