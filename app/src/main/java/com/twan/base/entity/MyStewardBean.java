package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/10
 */
public class MyStewardBean extends BaseBean {
    private List<StewardBean> list;
    private MyPage extra;

    public List<StewardBean> getList() {
        return list;
    }

    public void setList(List<StewardBean> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }
}
