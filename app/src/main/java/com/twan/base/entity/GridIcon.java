package com.twan.base.entity;

/**
 * @author Twan
 * @date 2019/3/22
 */
public class GridIcon {

    private int rid ;
    private String desc;

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public GridIcon(int rid, String desc) {
        this.rid = rid;
        this.desc = desc;
    }
}
