package com.twan.base.entity;

import java.io.Serializable;

/**
 * @author Twan
 * @date 2019/4/25
 */
public class TenementFeeBean implements Serializable {
    private String id;//": "2",
    private String title;//": "2019年3月-2019年6月份账单",
    private String money;//": "529.2",
    private String size;//": "126平米",
    private String danjia;//": "1.6元/平米/月",
    private String zhouqi;//": "2019.03.01-2019.06.15",
    private String yueNum;//": "3.5",
    private String sum;//": "126*1.6*3.5=705.6元"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDanjia() {
        return danjia;
    }

    public void setDanjia(String danjia) {
        this.danjia = danjia;
    }

    public String getZhouqi() {
        return zhouqi;
    }

    public void setZhouqi(String zhouqi) {
        this.zhouqi = zhouqi;
    }

    public String getYueNum() {
        return yueNum;
    }

    public void setYueNum(String yueNum) {
        this.yueNum = yueNum;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }
}
