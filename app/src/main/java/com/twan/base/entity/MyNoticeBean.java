package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/8
 */
public class MyNoticeBean extends BaseBean {

    private List<HouseNotiBean> list;
    private MyPage extra;

    public List<HouseNotiBean> getList() {
        return list;
    }

    public void setList(List<HouseNotiBean> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }
}
