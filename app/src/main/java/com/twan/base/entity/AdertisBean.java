package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/8
 */
public class AdertisBean extends BaseBean {

    private List<Adv> list;
    private MyPage extra;

    public List<Adv> getList() {
        return list;
    }

    public void setList(List<Adv> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }

    public static class Adv{
        private String ggId;//": "2",
        private String title;//": "广告标题",
        private String img;//": "http://wuye.gugangkf.cn/Upload/2.jpg",
        private String url;//": "https://www.baidu.com/"

        public String getGgId() {
            return ggId;
        }

        public void setGgId(String ggId) {
            this.ggId = ggId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
