package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/9
 */
public class MyMessage extends BaseBean {
    private List<MessageBean> list;
    private MyPage extra;

    public List<MessageBean> getList() {
        return list;
    }

    public void setList(List<MessageBean> list) {
        this.list = list;
    }

    public MyPage getExtra() {
        return extra;
    }

    public void setExtra(MyPage extra) {
        this.extra = extra;
    }
}
