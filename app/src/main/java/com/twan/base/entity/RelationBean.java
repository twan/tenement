package com.twan.base.entity;

import java.util.List;

/**
 * @author Twan
 * @date 2019/5/6
 */
public class RelationBean extends BaseBean {
    private List<Relation> list;

    public List<Relation> getList() {
        return list;
    }

    public void setList(List<Relation> list) {
        this.list = list;
    }

    public static class Relation {
        private String gxId;//": "2",
        private String gxName;//": "母亲"

        public String getGxId() {
            return gxId;
        }

        public void setGxId(String gxId) {
            this.gxId = gxId;
        }

        public String getGxName() {
            return gxName;
        }

        public void setGxName(String gxName) {
            this.gxName = gxName;
        }
    }
}
