package com.twan.base.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.twan.base.R;

/**
 * Description: 仿知乎底部评论弹窗
 * Create by dance, at 2018/12/25
 */
public class SelectHeadPopup extends BottomPopupView {

    private SelectPhoto mListener;
    public SelectHeadPopup(@NonNull Context context , SelectPhoto listener) {
        super(context);
        this.mListener = listener;
    }
    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_select_photo;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.take).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.take();
                    SelectHeadPopup.this.dismiss();
                }
            }
        });

        findViewById(R.id.photo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.select();
                    SelectHeadPopup.this.dismiss();
                }
            }
        });

        findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectHeadPopup.this.dismiss();
            }
        });
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {

    }

    @Override
    protected int getMaxHeight() {
        return (int) (XPopupUtils.getWindowHeight(getContext())); //*.85f
    }

    public interface SelectPhoto{
        void take();
        void select();
    }
}