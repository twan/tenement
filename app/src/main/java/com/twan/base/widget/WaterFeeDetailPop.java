package com.twan.base.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.lxj.xpopup.animator.PopupAnimator;
import com.lxj.xpopup.core.CenterPopupView;
import com.twan.base.R;
import com.twan.base.entity.TenementFeeBean;
import com.twan.base.entity.WaterFeeBean;

/**
 * @author Twan
 * @date 2019/4/19
 */
public class WaterFeeDetailPop extends CenterPopupView {
    //自定义弹窗本质是一个自定义View，但是只需重写这个构造，其他的不用重写
    WaterFeeBean data;
    public WaterFeeDetailPop(@NonNull Context context, WaterFeeBean model) {
        super(context);
        data = model;
    }
    // 返回自定义弹窗的布局
    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_waterfee_detail;
    }
    // 执行初始化操作，比如：findView，设置点击，或者任何你弹窗内的业务逻辑
    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_last = findViewById(R.id.tv_last);
        TextView tv_this = findViewById(R.id.tv_this);
        TextView tv_zhouqi = findViewById(R.id.tv_zhouqi);
        TextView tv_danjia = findViewById(R.id.tv_danjia);
        TextView tv_total = findViewById(R.id.tv_total);

        tv_last.setText(data.getBiao1());
        tv_this.setText(data.getBiao2());
        tv_zhouqi.setText(data.getZhouqi());
        tv_danjia.setText(data.getDanjia());
        tv_total.setText(data.getSum());
    }
    // 设置自定义动画器，看需要而定
    @Override
    protected PopupAnimator getPopupAnimator() {
        return super.getPopupAnimator();
    }
}
