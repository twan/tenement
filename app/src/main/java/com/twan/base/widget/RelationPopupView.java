package com.twan.base.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.twan.base.R;
import com.twan.base.adapter.RelationGridViewAdapter;
import com.twan.base.entity.RelationBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelationPopupView extends PartShadowPopupView {

    @BindView(R.id.gridview) GridView gridView;
    List<RelationBean.Relation> mGridData;
    private IItemClick mListener;

    public RelationPopupView(@NonNull Context context,List<RelationBean.Relation> data,IItemClick listener) {
        super(context);
        mListener = listener;
        mGridData = data;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_relation;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        ButterKnife.bind(this);
        initGridView();
    }

    private void initGridView(){
        gridView.setAdapter(new RelationGridViewAdapter(getContext(),R.layout.item_grid_relation,mGridData));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RelationBean.Relation stype = mGridData.get(position);
                if (mListener != null){
                    mListener.selected(stype);
                }
                RelationPopupView.this.dismiss();
            }
        });
    }

    public interface IItemClick{
        void selected(RelationBean.Relation bean);
    }
}
