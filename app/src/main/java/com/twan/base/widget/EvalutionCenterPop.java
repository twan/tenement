package com.twan.base.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.lxj.xpopup.animator.PopupAnimator;
import com.lxj.xpopup.core.CenterPopupView;
import com.twan.base.R;
import com.twan.base.api.Api;
import com.twan.base.entity.BaoxiuDetailBean;
import com.twan.base.network.ZyCallBack;
import com.twan.base.ui.StewardEvalutionActivity;
import com.twan.base.util.SPUtils;
import com.twan.base.util.ToastUtil;

/**
 * @author Twan
 * @date 2019/4/19
 */
public class EvalutionCenterPop extends CenterPopupView {
    //自定义弹窗本质是一个自定义View，但是只需重写这个构造，其他的不用重写
    String mtext="";
    String pjdj="1";
    IEvalution mListener;

    EditText edt_memo;
    Button btn;
    CheckBox chb_great;
    CheckBox chb_common;
    CheckBox chb_unsta;

    public EvalutionCenterPop(@NonNull Context context, String text, IEvalution listener) {
        super(context);
        mtext = text;
        mListener = listener;
    }
    // 返回自定义弹窗的布局
    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_evalution;
    }
    // 执行初始化操作，比如：findView，设置点击，或者任何你弹窗内的业务逻辑
    @Override
    protected void onCreate() {
        super.onCreate();
        edt_memo = findViewById(R.id.edt_memo);
        btn = findViewById(R.id.btn_commit);
        chb_great = findViewById(R.id.chb_great);
        chb_common = findViewById(R.id.chb_common);
        chb_unsta = findViewById(R.id.chb_unsta);

        chb_great.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chb_common.setChecked(false);
                    chb_unsta.setChecked(false);
                    pjdj ="1";
                }
            }
        });
        chb_common.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chb_great.setChecked(false);
                    chb_unsta.setChecked(false);
                    pjdj ="2";
                }

            }
        });
        chb_unsta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chb_great.setChecked(false);
                    chb_common.setChecked(false);
                    pjdj ="3";
                }

            }
        });

        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String memo = edt_memo.getText().toString();
                if(TextUtils.isEmpty(memo)){
                    ToastUtil.warn("请输入内容");
                    return;
                }
                Api.getApiService().addPj(SPUtils.getCustomId(),mtext,pjdj,memo).enqueue(new ZyCallBack() {
                    @Override
                    public void doSuccess() {
                        ToastUtil.success("评价成功");
                        if (mListener != null){
                            mListener.onsucess();
                        }
                        EvalutionCenterPop.this.dismiss();
                    }
                });
            }
        });
    }
    // 设置自定义动画器，看需要而定
    @Override
    protected PopupAnimator getPopupAnimator() {
        return super.getPopupAnimator();
    }

    public interface IEvalution{
        void onsucess();
    }
}
